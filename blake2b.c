#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "Hacl_Hash.h"

void print_vector(const uint8_t *buf, size_t size)
{
    for (size_t i = 0; i < size; i++) {
        printf("%x%x", buf[i] >> 4, buf[i] & 0x0f);
    }
    printf("\n");
}


// gcc  -I../kremlin/include -I../kremlin/kremlib/dist/minimal blake2b.c Hacl_Hash.c Hacl_Blake2b_32.c Lib_Memzero0.c Hacl_Blake2s_32.c
int main(void) {
	uint8_t hash[64];
	uint8_t in[64];
	uint8_t key[64];

	for (size_t i = 0; i < 64; i++) {
		in[i]  = i;
		key[i] = i;
	}
	// WRONG
	// https://github.com/BLAKE2/BLAKE2/blob/master/testvectors/blake2b-kat.txt#L5
    Hacl_Blake2b_32_blake2b(64, hash, 0, in, 64, key);
    print_vector(hash, 64);

	// OK
	// https://github.com/BLAKE2/BLAKE2/blob/master/testvectors/blake2b-kat.txt#L9
    Hacl_Blake2b_32_blake2b(64, hash, 1, in, 64, key);
    print_vector(hash, 64);

	return 0;
}
